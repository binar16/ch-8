import React, { Fragment, useState } from "react";
import Form from "../../compenent/Form";
import Tabel from "../../compenent/Tabel";
import Create from "../../compenent/Create/Create";
import 'bootstrap/dist/css/bootstrap.min.css'

const MainLayout = () => {
  const [list, setList] = useState([]);

  const [input, setInput] = useState({
    username: "",
    email: "",
    experience: "",
    level: "",
  });
  const [lastId, setLastId] = useState(0);
  const [filterList, setFilterList] = useState(null);
  const [isFilterList, setIsFilterList] = useState(false);
  const [createList, setCreateList] = useState("LIST");

  const filterListHandler = (todo) => {
    const { username, email, experience, level } = todo;
    if (!username && !email && !experience && !level) {
      alert("harap isi input");
      return;
    }

    const newFilterList = list.filter((player) => {
      return (
        username === player.username ||
        email === player.email ||
        experience === player.experience ||
        level === player.level
      );
    });
    if (newFilterList) {
      setFilterList(newFilterList);
      setIsFilterList(true);
      return;
    }
    if (newFilterList.length == 0) {
      alert("player tidak ada");
      return;
    }
  };

  const addListHandler = (todo) => {
    const id = lastId + 1;
    const updatedTodo = {
      id,
      ...todo,
    };
    const newTodos = [...list, updatedTodo];

    setList(newTodos);
    setLastId(id);
    setCreateList("LIST");
    console.log(list);
  };

  const changeHandler = (todo, value) => {
    const newTodo = { ...input };
    newTodo[todo] = value;
    setInput(newTodo);
  };

  const updateListHandler = () => {
    const isEdit = {
      id: filterList,
      username: input.username,
      email: input.email,
      experience: input.experience,
      level: input.level,
    };

    const newEdit = [...list];
    const index = list.findIndex((list) => list.id === filterList);
    newEdit[index] = isEdit;
    setList(newEdit);
    setCreateList("LIST");
  };

  const editListHandler = (list) => {
    setCreateList("EDIT");
    setFilterList(list.id);
    const isInput = {
      username: list.username,
      email: list.email,
      experience: list.experience,
      level: list.level,
    };
    setInput(isInput);
  };

  const backList = () => {
    setCreateList("LIST");
  };
  const backFilter = () => {
    setIsFilterList(false);
  };

  return (
    <Fragment>
      {createList === "LIST" && (
        <div className="bg-secondary d-flex gap-2 p-5 h-100">
          <div className="col-4 bg-white rounded text-center">
            <Form onAdd={filterListHandler} />
            {isFilterList === true &&(
              <button className="btn btn-warning" onClick={backFilter}>
                back
              </button>
            )}
          </div>
          <div className="bg-white rounded col-8 d-flex p-3 flex-column">
            <button
              className="btn btn-success  d-flex align-self-end"
              onClick={() => setCreateList("CREATE")}
            >
              create player
            </button>
            {isFilterList ? (
              <Tabel list={filterList} />
            ) : (
              <Tabel list={list} onEdit={editListHandler} />
            )}
          </div>
        </div>
      )}
      {createList === "CREATE" && (
        <Create onAdd={addListHandler} back={backList} />
      )}
      {createList === "EDIT" && (
        <div className="main">
          <div className="box shadow">
            <div className="p-3">
              <div>
                <label className="form-label">username</label>
                <input
                  className="form-control"
                  value={input.username}
                  onChange={(e) => changeHandler("username", e.target.value)}
                />
              </div>
              <div className="form">
                <label className="form-label">email</label>
                <input
                  className="form-control"
                  value={input.email}
                  onChange={(e) => changeHandler("email", e.target.value)}
                />
              </div>
              <div className="form">
                <label className="form-label">level</label>
                <input
                  className="form-control"
                  value={input.level}
                  onChange={(e) => changeHandler("level", e.target.value)}
                />
              </div>
              <div className="form">
                <label className="form-label">experience</label>
                <input
                  className="form-control"
                  value={input.experience}
                  onChange={(e) => changeHandler("experience", e.target.value)}
                />
              </div>
              <br />

              <button
                type="submit"
                className="btn btn-warning "
                onClick={() => backList()}
              >
                back
              </button>

              <button
                type="submit"
                className="btn btn-primary"
                onClick={updateListHandler}
                disabled={!input}
              >
                simpan
              </button>
            </div>
          </div>
        </div>
      )}
      ;
    </Fragment>
  );
};

export default MainLayout;
