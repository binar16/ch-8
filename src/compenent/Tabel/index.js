const Tabel = (props) => {
  return (
    <div className="container">
      <table className="table">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Username</th>
            <th scope="col">Email</th>
            <th scope="col">Level</th>
            <th scope="col">Experience</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
          {props.list.map((list) => (
            <tr>
              <th scope="row">{list.id}</th>
              <td>{list.username}</td>
              <td>{list.email}</td>
              <td>{list.level}</td>
              <td>{list.experience}</td>
              <td>
                <button
                  className="btn btn-warning"
                  onClick={() => props.onEdit(list)}
                >
                  Edit
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default Tabel;
