import React, { useState } from "react";

const Form = (props) => {
  const [input, setInput] = useState({
    username: "",
    email: "",
    experience: "",
    level: "",
  });

  const changeHandler = (todo, value) => {
    const newTodo = { ...input };
    newTodo[todo] = value;
    setInput(newTodo);
  };
  return (
    <div className="p-3">
      <div>
        <label className="form-label">Username</label>
        <input
          className="form-control"
          value={input.username}
          onChange={(e) => changeHandler("username", e.target.value)}
        />
      </div>
      <div>
        <label className="form-label">Email</label>
        <input
          className="form-control"
          value={input.email}
          onChange={(e) => changeHandler("email", e.target.value)}
        />
      </div>
      <div>
        <label className="form-label">Level</label>
        <input
          className="form-control"
          value={input.level}
          onChange={(e) => changeHandler("level", e.target.value)}
        />
      </div>
      <div>
        <label className="form-label">Experience</label>
        <input
          className="form-control"
          value={input.experience}
          onChange={(e) => changeHandler("experience", e.target.value)}
        />

      </div>
        <button className="btn btn-primary mt-3" onClick={() => props.onAdd(input)}>
          submit
        </button>
    </div>
  );
};

export default Form;
