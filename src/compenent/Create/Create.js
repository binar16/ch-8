import React from "react";
import { useState } from "react";
import "../../App.css";

const Create = (props) => {
  const [input, setInput] = useState({
    username: "",
    email: "",
    experience: "",
    level: "",
  });

  const changeHandler = (todo, value) => {
    const newTodo = { ...input };
    newTodo[todo] = value;
    setInput(newTodo);
  };

  return (
    <div className="main">
      <div className="box shadow">
        <div className="p-3">
          <div >
            <label className="form-label">username</label>
            <input
              className="form-control"
              value={input.username}
              onChange={(e) => changeHandler("username", e.target.value)}
            />
          </div>
          <div className="form">
            <label className="form-label">email</label>
            <input
              className="form-control"
              value={input.email}
              onChange={(e) => changeHandler("email", e.target.value)}
            />
          </div>
          <div className="form">
            <label className="form-label">level</label>
            <input
              className="form-control"
              value={input.level}
              onChange={(e) => changeHandler("level", e.target.value)}
            />
          </div>
          <div className="form">
            <label className="form-label">experience</label>
            <input
              className="form-control"
              value={input.experience}
              onChange={(e) => changeHandler("experience", e.target.value)}
            />
          </div>
          <br />

          <button
            type="submit"
            className="btn btn-warning "
            onClick={() => props.back()}
          >
            back
          </button>
          
          <button
            type="submit"
            className="btn btn-primary"
            onClick={() => props.onAdd(input)}
            disabled={!input}
          >
            simpan
          </button>
        </div>
      </div>
    </div>
  );
};

export default Create;
